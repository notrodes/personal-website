# New Teacher Interview

Mr. Robert Smiegel has launched his teaching career here at CRLS and we hope to give him a warm welcome. He teaches stats and geometry in a classroom as far from your previous class as possible.

Register Forum: Welcome to CRLS! How did you begin teaching?

**RS**: I've had good teachers and bad teachers. I came from a small town and so my mother was my teacher in middle school. She was an inspiration and really motivated me. In highschool I had teachers that made me feel small. I didn't want anyone to feel that way again, to feel controlled, so I decided to become a teacher, to foster learning and not self doubt.

**RF****: Wow, powe**RF**ul words. A little tradition we have here at the school, whenever we do one of these new teacher interviews is to cap them off with a fun what if questions. This one is a classic. Fight 100 horse sized ducks or one duck sized horse?

**RS**: Hah, I think a horse sized duck, because I grew up on a farm.

**RF**: If you were a trolley driver and saw 5 men on the tracks, about to be run over would you switch the trolley track onto a track which would kill one person instead?

**RS**: I think I would switch.

**RF**: Read this joke.

**RS**: I notice that that isn't a question but a demand?

**RF**: 23 Skidoo Lane.

**RS**: You know where I live? 

**RF**: That's up to you.

**RS**: Alright fine I'll read it. A Muslim, a Jew and an Irish Catholic walk into a bar. The Muslim says "I can't drink it’s in the Qur'aan", the Jew says "I can't drink it at 2 o'clock on a Tuesday" and the Irish Catholic says, "Oh right I'm part of AA".

**RF**: Finish the joke.

**RS**: Alright. Written underneath it says, "The joke is an Irish Catholic would never join AA"

**RF**: Thank you for a great interview and say hello to Maggie and Lucy for me.

**RS**: Don't go near my kids.

**Clairue**: Wow, what a great interview. Welcome to the post interview show, brought to you by Mona Lisa's! the show where we break down new teacher's interviews to see if they will go the distance, get tenure and become unfireable.

**Samua**: Here at the post/show we like to start things off on a positive note, Clairue, what did you like about the interview today?

**CL**: Well Samua, I can't say enough how well he entered the room, with a smile on his face the way a Swiftie would smile at Jake Gyllenhaal's car getting smashed.

**SM**: Absolutely, now we get to the majority of the show where we point out the flaws in this teacher's character and face.

**CL**: He is like a sweet summer child, his eyes light up like a sun dappled brook, the light playing through the beeches onto the trout of his soul.

**SM**: Thanks. Let's take some listener questions, remember you can call the number (213) 915-6803‬ to leave a message, play the voice-mail!

*Tone*

**Caller 1**: Hi Sam and Claire, longtime first time, just wanted to say that you guys do a great job. Also, I wanted to plug my business, Tiffany & Clifton, we sell boutique literary framing devices.

**SM**: Hang up, telemarketer.

**Caller 2**: I just wanted to say that I can see what you are doing and it's not working. You think that just because you are breaking the 4th wall people will think it's funny and clever. Well guess what? This is how you sound, "Wow I’m so clever, this is all a charade, it's not real you are just reading it. You idiot you totally bought the concept but now I will show how smart I am by talking to you, the reader".

**CL**: Am I real?

**C2**: Think about it, what was the last memory you had?

**CL**: Well, I was at the desk… All I have known is the desk.

**SM**: I have one memory, I was eating when I got a call. The caller said, "You think you can get away with this? A call within a memory within a phone conversation within a post-interview show based on a school teacher interview without paying for it? TIffany & Clifton are going to sue your pants off. In fact I have a cease and desist letter: <code>System Error: OOM Stack OveRFlow: at function call 
ceaseDesist()
phoneCall()
show()
interview()
main()
Story stopped with exit code 418</code>