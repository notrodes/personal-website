# Career Search

What could be better than helping someone achieve their dreams? Angel Upp, a talented CRLS senior was recently recruited by an international public relations company based in Boston, to do a co-op and realize their hopes of becoming a copy-writer as part of a new Cambridge program to promote youth jobs and career success. I for the story I talked with all involved, Garrett Jones, the Chief Creative Officer at the participating company and Angel Upp.

That's something that Upp says drew her to apply for this internship; "I first discovered my passion for the topic when I was 7, other kids wanted to be astronauts or the President, I wanted to show that Volkswagen's admissions incident was just an overwhelming desire to deliver to the consumer a superior product".

I asked Mr. Jones about some of the challenges and rewarding aspects of the job.

“Working in public relations at a company is challenging, but rewarding.”

Do you think this job requires a lot of creativity or collaboration?

“Brainstorming for our clients requires a lot of creativity and collaboration. Sometimes working with a client can be difficult, but then you get to the end and take a step back and look at a finished product, a poster, press conference, or ad campaign, and become so proud of the final product.”

Do you enjoy the creative nature of the job, coming into the office every day to just let your pen run wild, to play with words?

"Yes".

Can you give me a concrete example of what your department does?

“We recently were hired by the Taliban. When we first sat down we started with their mission statement: “Impose a strict interpretation of Sharia law.”The first thing we always think about is, “Okay, how do we pivot this brand onto TikTok?” The answer came to them quickly Jones said with a smile, "We created 3 dances  and a handful of makeup tutorials that they could immediately begin rolling out through their insurgent forces. They were a secret weapon. This is the type of work that will be forever associated with the company".

I asked Upp what her first reaction was when she learned that her application was accepted.

“As kind as the offer was, it was impossible for me to accept the position due to moral conflicts with the company. But I had confidence in myself, confidence that my passion and drive would allow me to find another job elsewhere, a job I could feel proud to do. I looked at the stipend and I start in two weeks." Which just goes to show, in this uplifting story that nothing is truly impossible.