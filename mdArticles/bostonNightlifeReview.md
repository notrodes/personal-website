# Boston Nightlife Review

We get reviews from REAL PEOPLE, right off the street to figure out the best places to head out every night in Boston! First the Register Forum goes to the story in the streets of Brooklyn, New York.

**Register Forum (RF)**: Ayy man how are you doing? Forget about it. What’s your name?

**Going To Have To Ask You To Get Away From The School (GTHTAYTGAFTS)**: Yeah, I’m Going To Have To Ask You To Get Away From The School.

**RF**: Traditional Italian name, right? Alright what are cool cats like you doing out at night? Hitting up on North End? Porter Square? Forget about it.

**GTHTAYTGAFTS**: I’m not telling you my name. Are you doing an Italian-American accent?

**RF**: I ask the questions here.

**Given the ongoing nature of the lawsuit between the Register Forum and the Bensonhurst Brooklyn school board, the rest of the interview has been redacted.**
 
Next, in solidarity with our southern brothers, this edition comes straight off the Boston-esqe streets of Panama City, Panama.

**RF**: Hola, ¿cómo te llamas?

**Marco Fuentes**: Yeah, hi, do you need directions? The library is right across the street.

**RF**: Oh wow your English is so good!

**MF**: Well, I went to school in the United States.

**RF**: I’ll just read the question in Spanish so it’s easier for you. When es no en la mañana, qué haceras? Fues al sur de end?

**MF**: That was barely Spanish.

**RF**: Wow, it seems like you have a tenacious grasp on both languages.

**MF**: Tenuous, you were trying to insult me. You meant tenuous. You invade to build a canal, you invade to wage your war on drugs and you make an attempt to insult me. In this battle of wits you should have armed yourself my friend.

**RF**: Oh really, you think the South End is getting too gentrified to be LIT?

**MF**: It seems what you lack in brains you lack equally in eyes and ears. Content to let the world go round without change while focusing on the farcical and unimportant. Wake up, who broke you and made you ask these questions?

**RF**: There’s a speakeasy below the Children’s Museum? Wow, how do you get in?

**Given the ongoing nature of this veiled attack by the editor, and succinct yet brilliant writing by the author, the rest of the bit has been cut.**