# Top Ten Wacky Zany Pranks:
1. Put cellophane on a trash can so that when your friend tries to throw something they always fail!
2. Tie a dollar bill to a string and make your friend chase it around!
3. Put red food coloring in the toilet tank to make your friend think they have colon cancer!
4. Make delicious chocolate chip cookies with tablespoons of vanilla instead of teaspoons of vanilla, the cookies won't have lived up to their full potential!
5. Call your friend Ulysses S. Grant instead of your friend's real name so they think they are deceased General and former president Grant.
6. Bribe or extort your friend’s general practitioner to give your friend a wacky colon cancer diagnosis! Concurrently take advantage of lax hospital cyber security to change their digital medical records.
7. Gift your friend a white elephant.
8. Incorporate uranium-234 into your friend's cornflakes to give them colon cancer within 20 years!
9. Set your friend's alarm 30 minutes forward!
10. Realize you have no friends.