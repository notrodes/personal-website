# An Un-Comprehensi(ve/ble) Guide to Finding Things

Don't panic. In this world we do not have a lot. Usually ourselves and sometimes we also have things. This is a guide to finding what you lost, written by a person who hasn't lost anything, except the plot of this article. I would keep this guide somewhere you won't lose it.

## Keys
Sometimes you just need to retrace your steps. Check in the door. In the future you should always keep the keys in the same place. Once I wore the same set of jeans for a month, keys stayed in the front pocket, problem solved. Maybe a hook on the wall would work too. Go get a hook.

## Dogs
First, if there is a PETA branch in your area check with the local crematorium. Once you’ve determined your dog is still alive, walk around with dog food and call the dog’s name. The details may vary depending on where you come from― when I was dog-sitting in Andover I had to walk through a neighborhood yelling Josiah while shaking a sashimi party platter. 

## Less Important Pets
Not man's best friend so the best they get is a lightning round:

* Cats: Buy a bird.
* Birds: Look up.
* Chameleons: Give up.
* Hampsters: Clean up.
* Llamas: Set out Andes nuts.
* Penguins: See Mr. Popper.

## Friends
If there is one thing I know about people it is that they love pranks. Good thing we compiled a list of pranks to pull and clubs to meet people at. Go find other people with your interests. You know what doesn't work? Anti-social social club, it's not really a club, it's the intersection between irony club and lobotomized consumer club.

## The Other Sock
You lost one of your socks. If you are the type of person who loses socks you probably lost one before. Just pair them up, even if they don't match. You think people will notice? We cut holes in perfectly good jeans. Everyone is far too enraptured in how they think other people will judge them to notice others. As long as the sock is alright, wear any sock and take it one foot at a time.